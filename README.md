# LTE-M Library for dokode-Modem

「どこでもでむ」で使用できるLTE-Mモジュール用ドライバです。

**主な注意点**

使用モジュール:[nordic semiconductor nRF9160](https://www.nordicsemi.com/products/nrf9160)

内蔵ファームウェアは、Serial LTE modemを使用しています。
https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/applications/serial_lte_modem/README.html

コールドスタート時のハード初期化待ち時間：　約１０秒

アクティベーション：　５秒から２０秒ぐらい。成功しなかったらリトライが必要。

通信速度：　LTE-M自体は300kbpsだが、UARTの115200bpsとコマンドのやり取りがリミットとなる。

インターネットのベストエフォートにより、連続送受信に時間がかかるときがあります。

暗号化通信：　可能。暗号鍵を用意する必要があります。


どこでもでむMiniを使用する場合はファイルの修正とオプションの追加が必要です。下記を参照してください。

https://gitlab.com/dokodemodem/libraries/dokodemodem_mini


LTE-Mを使う場合は、platformio.iniに下記を追記してください。
```
build_flags = -DUSE_LTE_M
```
<br>

**通信できない場合は、不具合個所の切り分けをお願いします。**

①SIM設定はあってますか？

SIMによっては開通処理が必要なものもあります。開通されていて使える状態なのか確認ください。

ユーザー名、パスワード、APNがSIMに合っているか確認しましょう。

②サーバー用の設定はあってますか？

URLや、ユーザーIDやパスワード、必要な暗号鍵は正しく設定されているでしょうか？

同じ値を使って、別のデバイス（例：ESP32をつかったWiFi系Arduino）でもアクセスできるでしょうか？

③ハード的な確認事項

どこでもでむの電源は外部電源からとっているでしょうか？USBからだけでは通信できません。

アンテナはつながっているでしょうか？

LTM-1モジュールを単独で使う場合、VCCに電源を入れるのとVREFにもUARTと同じ電圧を入力する必要があります。
