/*
  Based programs are as follow
  https://github.com/SeeedJP/Wio_cell_lib_for_Arduino

  Modified by CircuitDesign,Inc.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#pragma once
#include <Arduino.h>

class Stopwatch
{
private:
	unsigned long _BeginTime;
	unsigned long _EndTime;

public:
	Stopwatch() : _BeginTime(0), _EndTime(0)
	{
	}

	void Restart()
	{
		_BeginTime = millis();
		_EndTime = 0;
	}

	void Stop()
	{
		_EndTime = millis();
	}

	unsigned long ElapsedMilliseconds() const
	{
		if (_EndTime == 0) {
			return millis() - _BeginTime;
		}
		else {
			return _EndTime - _BeginTime;
		}
	}

};
