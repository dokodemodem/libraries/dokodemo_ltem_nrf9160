/*
  nRF9160ModemClient.cpp - Client class for HL7800(Sierra Wireless)
  Copyright (c) 2020 CircuitDesign,Inc.  All right reserved.

  Based programs are as follow
  https://github.com/SeeedJP/Wio_cell_lib_for_Arduino

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#include "nRF9160ModemClient.h"

#define RECEIVE_MAX_LENGTH (2000)

#define CONNECT_SUCCESS (1)

nRF9160ModemClient::nRF9160ModemClient(nRF9160Modem *ltem)
{
    _ltem = ltem;
    _sessionID = -1;
    _ReceiveBuffer = new byte[RECEIVE_MAX_LENGTH];
    _ringBuffer = new byte[RECEIVE_MAX_LENGTH];
    head = tail = data_length = 0;
}

nRF9160ModemClient::~nRF9160ModemClient()
{
    delete[] _ReceiveBuffer;
    delete[] _ringBuffer;
}

int nRF9160ModemClient::connect(IPAddress ip, uint16_t port)
{
    String ipStr = String(ip[0]);
    ipStr += ".";
    ipStr += String(ip[1]);
    ipStr += ".";
    ipStr += String(ip[2]);
    ipStr += ".";
    ipStr += String(ip[3]);

    int sessionID = _ltem->socketOpen(ipStr.c_str(), port, nRF9160Modem::SOCKET_TCP);
    if (sessionID < 0)
        return 0;

    _sessionID = sessionID;

    return CONNECT_SUCCESS;
}

int nRF9160ModemClient::connect(const char *host, uint16_t port)
{
    int sessionID = _ltem->socketOpen(host, port, nRF9160Modem::SOCKET_TCP);
    if (sessionID < 0)
        return 0;

    _sessionID = sessionID;

    return CONNECT_SUCCESS;
}

size_t nRF9160ModemClient::write(uint8_t data)
{
    if (_sessionID < 0)
        return 0;

    if (!_ltem->socketSend(_sessionID, &data, 1))
    {
        return 0;
    }

    return 1;
}

size_t nRF9160ModemClient::write(const uint8_t *buf, size_t size)
{
    if (_sessionID < 0)
        return 0;

    if (!_ltem->socketSend(_sessionID, buf, size))
    {
        return 0;
    }

    return size;
}

int nRF9160ModemClient::GetDataLength()
{
    if (tail >= head)
        return tail - head;
    else
        return tail - head + RECEIVE_MAX_LENGTH;
}

byte nRF9160ModemClient::pop()
{
    byte ret = _ringBuffer[head];
    head = (head + 1) % RECEIVE_MAX_LENGTH;
    data_length = GetDataLength();

    return ret;
}

int nRF9160ModemClient::available()
{
    if (_sessionID < 0)
        return 0;

    if (data_length > 0)
    {
        return data_length;
    }

    int receiveSize = _ltem->socketReceive(_sessionID, _ReceiveBuffer, RECEIVE_MAX_LENGTH);

    for (int i = 0; i < receiveSize; i++)
    {
        // SerialDebug.print((char) _ReceiveBuffer[i]);
        *(_ringBuffer + tail++) = _ReceiveBuffer[i];
        tail %= RECEIVE_MAX_LENGTH;
    }
    data_length = GetDataLength();

    return data_length;
}

//int xCount = 352;
int nRF9160ModemClient::read()
{
    if (_sessionID < 0)
        return -1;

    int actualSize = available();
    //if (actualSize < xCount)
    //    SerialDebug.println(String(actualSize));

    if (actualSize <= 0)
        return -1; // None is available.

    return pop();
}

int nRF9160ModemClient::read(uint8_t *buf, size_t size)
{
    if (_sessionID < 0)
        return -1;

    int actualSize = available();
    if (actualSize <= 0)
        return -1; // None is available.

    int popSize = actualSize <= (int)size ? actualSize : size;
    for (int i = 0; i < popSize; i++)
    {
        buf[i] = pop();
    }

    return popSize;
}

int nRF9160ModemClient::peek()
{
    if (_sessionID < 0)
        return -1;

    int actualSize = available();
    if (actualSize <= 0)
        return -2; // None is available.

    return _ringBuffer[head];
}

void nRF9160ModemClient::flush()
{
    // Nothing to do.
}

void nRF9160ModemClient::stop()
{
    if (_sessionID < 0)
        return;

    _ltem->socketClose(_sessionID);
    _sessionID = -1;

    head = tail = data_length = 0;
}

uint8_t nRF9160ModemClient::connected()
{
    if (_sessionID < 0)
        return 0;

    return _ltem->checkConnetion(_sessionID);
}

nRF9160ModemClient::operator bool()
{
    return _sessionID >= 0 ? true : false;
}

bool nRF9160ModemClient::setCACert(const char *rootCA)
{
    return _ltem->setCACert(rootCA);
}

bool nRF9160ModemClient::setCertificate(const char *client_ca)
{
    return _ltem->setCertificate(client_ca);
}

bool nRF9160ModemClient::setPrivateKey(const char *private_key)
{
    return _ltem->setPrivateKey(private_key);
}
/*
String  nRF9160ModemClient::readStringUntil(char terminator)
{
  String ret;
  int c = read();
  while (c >= 0 && (char)c != terminator)
  {
    ret += (char)c;
    c = read();
  }
  return ret;
}*/