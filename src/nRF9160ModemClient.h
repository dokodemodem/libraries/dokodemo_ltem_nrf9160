#pragma once

#include "nRF9160Modem.h"
#include <Client.h>

class nRF9160ModemClient : public Client
{

protected:
    nRF9160Modem *_ltem;
    int _sessionID;
    byte *_ReceiveBuffer;

    byte *_ringBuffer;
    int head, tail, data_length;
    int GetDataLength();
    byte pop();

public:
    nRF9160ModemClient(nRF9160Modem *ltem);
    virtual ~nRF9160ModemClient();

    virtual int connect(IPAddress ip, uint16_t port);
    virtual int connect(const char *host, uint16_t port);
    virtual size_t write(uint8_t data);
    virtual size_t write(const uint8_t *buf, size_t size);
    virtual int available();
    virtual int read();
    virtual int read(uint8_t *buf, size_t size);
    virtual int peek();
    virtual void flush();
    virtual void stop();
    virtual uint8_t connected();
    virtual operator bool();

    bool setCACert(const char *rootCA);
    bool setCertificate(const char *client_ca);
    bool setPrivateKey(const char *private_key);
};
