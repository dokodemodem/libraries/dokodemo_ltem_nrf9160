#pragma once

#include <dokodemo.h>

#include <IPAddress.h>
#include <vector>
// #include <string>
#include <map>
#include <sstream>
// #include <locale>
#include <iomanip>
#include <chrono>
// #include <ctime>

// #define DEBUG_MODEM

#ifdef DEBUG_MODEM
#define DEBUG_PRINTLN(msg) SerialDebug.println(msg)
#define DEBUG_PRINT(msg) SerialDebug.print(msg)
#else
#define DEBUG_PRINTLN(msg)
#define DEBUG_PRINT(msg)
#endif

#define ERR_DEBUG_PRINTLN(msg) SerialDebug.println(msg)
#define ERR_DEBUG_PRINT(msg) SerialDebug.print(msg)

#define MODEM_READ_TIMEOUT 500         // 500mSec
#define MODEM_SHORT_TIMEOUT 3000       // 3sec
#define MODEM_LONG_TIMEOUT 10000       // 10sec
#define MODEM_ACTIVATE_TIMEOUT 30000   // 30sec
#define MODEM_CONNECT_TIMEOUT 60000    // 60sec
#define MODEM_DISCONNECT_TIMEOUT 40000 // 40sec

#define READ_BYTE_TIMEOUT (10)
#define RESPONSE_MAX_LENGTH (128)

#define CHAR_CR (0x0d)
#define CHAR_LF (0x0a)

const char ATRSP_OK[] = "^OK$";

//#define USE_HWFLOW
#ifdef USE_HWFLOW
typedef int (*UartWrite)(const uint8_t *data, uint32_t len);
typedef int (*UarttRead)(uint8_t *data, uint32_t len, uint32_t timeout);
#endif

class nRF9160Modem
{
public:
    enum SocketType
    {
        SOCKET_TCP,
        SOCKET_UDP,
        SOCKET_HTTP,
    };

    enum Channel
    {
        USE_DOCOMO,
        USE_KDDI,
        USE_SOFTBANK
    };

    enum Mode
    {
        USE_TCP,
        USE_UDP,
    };

    enum State
    {
        NONE,
        CONNECTED,
        DISCONNECTED,
        RECEIVED,
        ERROR,
        BUFFOVERFLOW
    };

    nRF9160Modem();
    ~nRF9160Modem();

#ifdef USE_HWFLOW
    bool init(UartWrite pWriteFunc, UarttRead pReadFunc);
#else
    bool init(HardwareSerial &pUart);
#endif
    bool activate(const char *accessPointName, const char *userName, const char *password,Channel chan = Channel::USE_DOCOMO);
    bool deactivate();
    int16_t getRssi(void);
    bool getVersion(std::string *response);
    bool getlocaltime(std::tm *response, bool uselocal = true);
    bool getIPaddress(std::string *response);
    bool getUUID(std::string *response);
    bool getPhoneNumber(std::string *response);
    bool getIMSI(std::string *response);
    bool getBand(uint8_t *band);
    bool getCellRssi(int16_t *rssi);

    // for client
    int socketOpen(const char *host, int port, SocketType type = SocketType::SOCKET_TCP);
    bool socketSend(int sessionID, const byte *data, int dataSize);
    int socketReceive(int sessionID, byte *data, int dataSize);
    bool socketClose(int sessionID);
    bool checkConnetion(int sessionID);

    bool setCACert(const char *rootCA);
    bool setCertificate(const char *client_ca);
    bool setPrivateKey(const char *private_key);

    bool mqttOpen(const char *host, int port = 1881, const char *client_id = "", const char *username = "", const char *password = "");
    bool mqttClose();
    bool mqttSubscribe(const char *topic, int qos = 0);
    bool mqttUnSubscribe(const char *topic);
    bool mqttPublish(const char *topic, const char *msg, int qos = 0, int retain = 0);
    int mqttLoop();
    String mqttTopic;
    String mqttMessage;

    bool netServer(int port, Mode mode = Mode::USE_UDP);
    bool netServerClose();
    bool netClientConnect(const char *host, int port, Mode mode = Mode::USE_UDP);
    bool netClientClose();
    bool netSend(const uint8_t *data,int len);
    bool netSend(String msg);
    State netLoop(uint8_t *data,int *max_len);

private:
#ifdef USE_HWFLOW
    UartWrite _pWrite;
    UarttRead _pRead;
#else
    HardwareSerial *m_pUart;
    int _pWrite(const uint8_t *data, uint32_t len);
    int _pRead(uint8_t *data);
#endif

    char *_cmd;
    bool _SecureMode;
    Mode _mode;
    bool _activated;

    bool waitLteModem();
    bool readResponseUntilOk(unsigned long timeout);
    bool writeCommand(const char *command);
    bool readResponse(const char *pattern, unsigned long timeout, std::string *capture, bool WaitOK = true, bool checkNotIf = false);
    bool readLine(unsigned long timeout, std::string *capture);
    bool writeCommandAndWaitOK(const char *command, unsigned long timeout);
    bool writeCommandAndReadResponse(const char *command, const char *pattern, unsigned long timeout, std::string *capture = NULL);
};
