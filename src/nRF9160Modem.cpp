#include "nRF9160Modem.h"
#include "Stopwatch.h"
using namespace std;

// #define ECHOBACK
#ifdef ECHOBACK
#define ECHOBACK_WRITE(msg) SerialDebug.write(msg)
#define ECHOBACK_WRITE_LEN(msg, len) SerialDebug.write(msg, len)
#else
#define ECHOBACK_WRITE(msg)
#define ECHOBACK_WRITE_LEN(msg, len)
#endif

#define CMD_BUFF_SIZE (256)

static vector<string> split(const string &s, char delim)
{
    vector<string> elems;
    string item;
    for (char ch : s)
    {
        if (ch == delim)
        {
            if (!item.empty())
                elems.push_back(item);
            item.clear();
        }
        else
        {
            item += ch;
        }
    }
    if (!item.empty())
        elems.push_back(item);
    return elems;
}

#ifndef USE_HWFLOW
int nRF9160Modem::_pWrite(const uint8_t *data, uint32_t len)
{
    ECHOBACK_WRITE_LEN((uint8_t *)data, len);
    return m_pUart->write(data, len);
}
int nRF9160Modem::_pRead(uint8_t *data)
{
    if (m_pUart->available() > 0)
    {
        *data = m_pUart->read();
        ECHOBACK_WRITE_LEN(data, 1);
        return 1;
    }

    return 0;
}
#endif

nRF9160Modem::nRF9160Modem()
{
    _cmd = new char[CMD_BUFF_SIZE];

    _SecureMode = false;
    _activated = false;
}

nRF9160Modem::~nRF9160Modem()
{
    delete[] _cmd;
}

bool nRF9160Modem::writeCommand(const char *command)
{
    int len = strlen(command);
    _pWrite((uint8_t *)command, len);

    return true;
}

bool nRF9160Modem::readResponseUntilOk(unsigned long timeout)
{
    std::string response;

    Stopwatch sw;
    sw.Restart();

    while (true)
    {
        uint8_t rddata;
        int len = _pRead(&rddata);
        if (len > 0)
        {
            response.push_back((char)rddata);
        }
        else
        {
            if (sw.ElapsedMilliseconds() >= timeout)
            {
                return false;
            }
            // delay(1);
        }

        if (response.size() >= 2 && response.at(response.size() - 2) == CHAR_CR && response.at(response.size() - 1) == CHAR_LF)
        {
            DEBUG_PRINT(response.c_str());

            if (response.find("OK") != std::string::npos)
            {
                return true;
            }
            else if (response.find("ERROR") != std::string::npos)
            {
                return false;
            }

            response.clear();
        }
    }
}

bool nRF9160Modem::writeCommandAndWaitOK(const char *command, unsigned long timeout)
{
    if (!writeCommand(command))
        return false;
    return readResponseUntilOk(timeout);
}

bool nRF9160Modem::readResponse(const char *pattern, unsigned long timeout, std::string *capture, bool WaitOK, bool checkNotIf)
{
    std::string response;
    Stopwatch sw;
    sw.Restart();

    if (capture != NULL)
        capture->clear();

    while (true)
    {
        if (response.size() >= (size_t)(RESPONSE_MAX_LENGTH + 2))
        {
            return false;
        }

        uint8_t rddata;
        int len = _pRead(&rddata);

        if (len > 0)
        {
            response.push_back((char)rddata);
        }
        else
        {
            if (sw.ElapsedMilliseconds() >= timeout)
            {
                return false;
            }
        }

        if (response.size() >= 2 && response.at(response.size() - 2) == CHAR_CR && response.at(response.size() - 1) == CHAR_LF)
        {
            DEBUG_PRINT(response.c_str());

            if (WaitOK && response.find("OK") != std::string::npos)
            {
                return true;
            }
            else if (response.find("ERROR") != std::string::npos)
            {
                return false;
            }

            if (capture != NULL)
            {
                if (response.find(pattern) != std::string::npos)
                {
                    response.resize(response.size() - 2);

                    if (capture->size() != 0)
                        *capture += "_"; // add a splitter char for AT+KTCPCFG?

                    *capture += response;

                    if (!WaitOK)
                        return true;
                }
            }
            else
            {
                if (response.find(pattern) != std::string::npos)
                {
                    return true;
                }
            }

            response.clear();
        }
    }
}
bool nRF9160Modem::readLine(unsigned long timeout, std::string *capture)
{
    std::string response;
    Stopwatch sw;
    sw.Restart();

    if (capture != NULL)
        capture->clear();

    while (true)
    {
        if (response.size() >= (size_t)(RESPONSE_MAX_LENGTH + 2))
        {
            return false;
        }

        uint8_t rddata;
        int len = _pRead(&rddata);

        if (len > 0)
        {
            if (response.size() == 0 && (rddata == '\r' || rddata == '\n'))
            {
                continue;
            }
            response.push_back((char)rddata);
        }
        else
        {
            if (sw.ElapsedMilliseconds() >= timeout)
            {
                return false;
            }
        }

        if (response.size() > 2 && response.at(response.size() - 2) == CHAR_CR && response.at(response.size() - 1) == CHAR_LF)
        {
            response.resize(response.size() - 2);
            *capture = response;

            return true;
        }
    }
}

bool nRF9160Modem::writeCommandAndReadResponse(const char *command, const char *pattern, unsigned long timeout, std::string *capture)
{
    if (!writeCommand(command))
        return false;
    return readResponse(pattern, timeout, capture);
}

bool nRF9160Modem::waitLteModem()
{
    for (int i = 0; i < 10; i++)
    {
        if (writeCommandAndWaitOK("AT\r\n", MODEM_READ_TIMEOUT))
            return true;
        delay(500);
    }
    return false;
}

bool nRF9160Modem::activate(const char *accessPointName, const char *userName, const char *password,Channel chan)
{
    // Sets the device to minimum functionality
    if (!writeCommandAndWaitOK("AT+CFUN=4\r\n", MODEM_SHORT_TIMEOUT)) // 0->4 avoid overwrite of Flash memory.
    {
        ERR_DEBUG_PRINTLN("activate error 2");
        return false;
    }

    // set AP name
    snprintf(_cmd, CMD_BUFF_SIZE, "AT+CGDCONT=0,\"IP\",\"%s\"\r\n", accessPointName);
    if (!writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 3");
        return false;
    }

    // set auth
    snprintf(_cmd, CMD_BUFF_SIZE, "AT+CGAUTH=0,2,\"%s\",\"%s\"\r\n", userName, password);
    if (!writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 4");
        return false;
    }

    // Sets Unsolicited notification level
    if (!writeCommandAndWaitOK("AT+CEREG=2\r\n", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 5");
        return false;
    }

    // Sets power saving mode
    if (!writeCommandAndWaitOK("AT+CPSMS=0\r\n", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 6");
        return false;
    }

    // Sets eDRS mode
    if (!writeCommandAndWaitOK("AT\%XSYSTEMMODE=1,0,0,0\r\n", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 8");
        return false;
    }

    // Sets error display mode
    if (!writeCommandAndWaitOK("AT+CMEE=1\r\n", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 9");
        return false;
    }

#if 1
    if (chan == Channel::USE_DOCOMO)
    {
        if (!writeCommandAndWaitOK("AT+COPS=1,2,\"44010\"\r\n", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("activate error 10d");
            return false;
        }
    }
    else if (chan == Channel::USE_KDDI)
    {
        if (!writeCommandAndWaitOK("AT+COPS=1,2,\"44051\"\r\n", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("activate error 10k");
            return false;
        }
    }
    else if (chan == Channel::USE_SOFTBANK)
    {
        if (!writeCommandAndWaitOK("AT+COPS=1,2,\"44020\"\r\n", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("activate error 10s");
            return false;
        }
    }
    else
    {
        ERR_DEBUG_PRINTLN("activate error 10");
        return false;
    }
#else
    // Automatic network selection
    if (!writeCommandAndWaitOK("AT+COPS=0\r\n", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 10d");
        return false;
    }
#endif

    // start activation
    if (!writeCommandAndWaitOK("AT+CFUN=1\r\n", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 11");
        return false;
    }

    //  Wait registration "+CEREG: 2," mean try to connection, "+CEREG: 1," or "+CEREG: 5," mean success
    Stopwatch sw;
    sw.Restart();
    std::string response;

    while (true)
    {
        response = "";
        if (!readResponse("+CEREG:", MODEM_LONG_TIMEOUT, &response, false))
        {
            ERR_DEBUG_PRINTLN("activate error 12");
            return false;
        }

        if (response.find("1,") != std::string::npos || response.find("5,") != std::string::npos)
        {
            break;
        }

        if (sw.ElapsedMilliseconds() >= MODEM_ACTIVATE_TIMEOUT)
        {
            ERR_DEBUG_PRINTLN("activate error 12");
            return false;
        }
    }

    // Disable error message
    if (!writeCommandAndWaitOK("AT+CEREG=0\r\n", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 13");
        return false;
    }

    // Check attach state
    response = "";
    if (!writeCommandAndReadResponse("AT+CGATT?\r\n", "+CGATT:", MODEM_SHORT_TIMEOUT, &response))
    {
        ERR_DEBUG_PRINTLN("activate error 14");
        return false;
    }

    if (response.find("1") != std::string::npos)
    {
        _activated = true;
        return true;
    }

    return false;
}

bool nRF9160Modem::deactivate()
{
    if (!writeCommandAndWaitOK("AT+CFUN=0\r\n", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("deactivate error 1");
        return false;
    }

    return true;
}

int nRF9160Modem::socketOpen(const char *host, int port, SocketType type)
{
    bool ret = false;
    std::string response;
    if (_SecureMode)
    {
        ret = writeCommandAndReadResponse("AT#XSSOCKET=1,1,0,2222,2\r\n", "#XSSOCKET", MODEM_LONG_TIMEOUT, &response);
        if (!ret)
        {
            // ERR_DEBUG_PRINTLN("socketOpen error");
            return -1; // command error
        }
    }
    else
    {
        ret = writeCommandAndReadResponse("AT#XSOCKET=1,1,0\r\n", "#XSOCKET", MODEM_LONG_TIMEOUT, &response);
        if (!ret)
        {
            // ERR_DEBUG_PRINTLN("socketOpen error");
            return -1; // command error
        }
    }

    vector<string> vResp = split(response, ' ');
    vector<string> v = split(vResp[1], ',');
    int handle = atoi(v[0].c_str()); // start from 0

    if (handle < 0)
        return -2;

    snprintf(_cmd, CMD_BUFF_SIZE, "AT#XCONNECT=\"%s\",%d\r\n", host, port);
    ret = writeCommandAndReadResponse(_cmd, "#XCONNECT", MODEM_LONG_TIMEOUT, &response);
    if (!ret)
    {
        // ERR_DEBUG_PRINTLN("socketOpen error");
        if (_SecureMode)
        {
            writeCommandAndWaitOK("AT#XSSOCKET=0\r\n", MODEM_SHORT_TIMEOUT); // close socket
        }
        else
        {
            writeCommandAndWaitOK("AT#XSOCKET=0\r\n", MODEM_SHORT_TIMEOUT); // close socket
        }

        return -3; // connection error
    }

    return handle;
}

bool nRF9160Modem::socketSend(int handle, const byte *data, int dataSize)
{
    std::string response;

    snprintf(_cmd, CMD_BUFF_SIZE, "AT#XSOCKETSELECT=%d\r\n", handle);

    if (!writeCommandAndReadResponse(_cmd, "#XSOCKETSELECT", MODEM_SHORT_TIMEOUT, &response))
    {
        // ERR_DEBUG_PRINTLN("socketOpen error");// No socket
        return false;
    }

    // enter datamode
    if (!writeCommandAndWaitOK("AT#XSEND\r\n", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("xsend error");
        return false;
    }

    // send data
    _pWrite((uint8_t *)data, dataSize);

    // exit data mode
    writeCommand("+++");

    if (!readResponse("#XDATAMODE", MODEM_SHORT_TIMEOUT, &response, false))
    {
        ERR_DEBUG_PRINTLN("socketSend error"); // No socket
        return false;                          // command timeout
    }

    return true;
}

int nRF9160Modem::socketReceive(int handle, byte *data, int dataSize)
{
    std::string response;

    // recv timeout 1sec
    writeCommand("AT#XRECV=0,64\r\n"); // 64 mean is non-block.

    if (!readResponse("#XRECV", MODEM_SHORT_TIMEOUT, &response, false))
    {
        // ERR_DEBUG_PRINTLN("recv error");
        return 0; // command timeout
    }
    if (response.length() == 0)
    {
        return 0;
    }

    // message: #XRECV: x  (x is size)
    vector<string> vResp = split(response, ' ');
    int recvSize = atoi(vResp[1].c_str());
    if (dataSize < recvSize)
    {
        return -1;
    }

    dataSize = recvSize;

    while (recvSize)
    {
        uint8_t rddata;
        int len = _pRead(&rddata);
        if (len > 0)
        {
            *data++ = (byte)rddata;
            // SerialDebug.write(rddata);
            --recvSize;
        }
    }

    readResponseUntilOk(MODEM_SHORT_TIMEOUT);

    // SerialDebug.print("### Recv Size=" + String(dataSize));
    return dataSize;
}

bool nRF9160Modem::socketClose(int handle)
{
    snprintf(_cmd, CMD_BUFF_SIZE, "AT#XSOCKETSELECT=%d\r\n", handle);

    if (!writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT))
    {
        // ERR_DEBUG_PRINTLN("socketClose error");
        return false;
    }
    // if #XSOCKETSELECT message not come, handle is not exist. but do command as follow

    if (_SecureMode)
    {
        writeCommandAndWaitOK("AT#XSSOCKET=0\r\n", MODEM_SHORT_TIMEOUT);
        // #XSOCKET: 0,"closed"
    }
    else
    {
        writeCommandAndWaitOK("AT#XSOCKET=0\r\n", MODEM_SHORT_TIMEOUT);
        // #XSOCKET: 0,"closed"
    }

    return true;
}

bool nRF9160Modem::checkConnetion(int handle)
{
    std::string response;

    snprintf(_cmd, CMD_BUFF_SIZE, "AT#XSOCKETSELECT=%d\r\n", handle);

    if (!writeCommandAndReadResponse(_cmd, "#XSOCKETSELECT", MODEM_SHORT_TIMEOUT, &response))
    {
        // No socket
        return false;
    }

    writeCommand("AT#XRECV=0,66\r\n"); // 66 MSG_DONTWAIT|MSG_PEEK
    if (readResponse("#XRECV", MODEM_SHORT_TIMEOUT, &response, true))
    {
        if (response.length() != 0) // sometime get message #XRECV: 4 and OK
        {
            // SerialDebug.println("### ret OK 1");
            return true;
        }

        // false:ERROR mean connected,true:OK mean disconnected
        // SerialDebug.println("### ret NG 1");
        return false;
    }

    // SerialDebug.println("### ret OK 2");
    return true;
}

int16_t nRF9160Modem::getRssi(void)
{
    std::string response;
    if (!writeCommandAndReadResponse("AT+CESQ\r\n", "+CESQ", MODEM_LONG_TIMEOUT, &response))
    {
        // Ncommand error
        return 0;
    }

    // message: +CESQ: 99,99,255,255,11,41  11,41=rssi,ber
    vector<string> vResp = split(response, ',');

    int16_t rssi = atoi(vResp[5].c_str()) - 140;

    return rssi;
}

#ifdef USE_HWFLOW
bool nRF9160Modem::init(UartWrite pWriteFunc, UarttRead pReadFunc, Channel band)
{
    // m_pUart = &pUart;
    if (pReadFunc == nullptr || pWriteFunc == nullptr)
        return false;

    _pWrite = pWriteFunc;
    _pRead = pReadFunc;

    // check response
    if (!waitLteModem())
    {
        ERR_DEBUG_PRINTLN("init error 2");
        return false;
    }

    //  reset
    if (!writeCommandAndWaitOK("AT#XRESET\r\n", MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 0");
        return false;
    }

    std::string response;
    if (!readResponse("Ready", MODEM_SHORT_TIMEOUT, &response, false))
    {
        ERR_DEBUG_PRINTLN("init error 1");
        return 0; // command timeout
    }

    return true;
}
#else
bool nRF9160Modem::init(HardwareSerial &pUart)
{
    m_pUart = &pUart;

    m_pUart->setTimeout(5000);
    String ret = m_pUart->readStringUntil('\n'); // wait Ready

    m_pUart->setTimeout(1);
    // check response
    if (!waitLteModem())
    {
        ERR_DEBUG_PRINTLN("init error 1");
        return false;
    }
#if 1
    //  reset
    if (!writeCommandAndWaitOK("AT#XRESET\r\n", MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 2");
        return false;
    }

    std::string response;
    if (!readResponse("Ready", MODEM_SHORT_TIMEOUT, &response, false))
    {
        ERR_DEBUG_PRINTLN("init error 3");
        return 0; // command timeout
    }
#endif
    return true;
}
#endif

bool nRF9160Modem::setCACert(const char *rootCA)
{
    // delete the existing rootCA
    if (!writeCommandAndWaitOK("AT\%CMNG=3,2222,0\r\n", MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setCACert error");
        // return false;
    }

    char cmd[] = "AT\%CMNG=0,2222,0,\"";
    writeCommand(cmd);
    _pWrite((uint8_t *)rootCA, strlen(rootCA));
    _pWrite((uint8_t *)"\"\r\n", 3);

    if (!readResponseUntilOk(MODEM_SHORT_TIMEOUT))
        return false;

    _SecureMode = true;
    return true;
}

bool nRF9160Modem::setCertificate(const char *client_ca)
{

    // delete the existing certificate
    if (!writeCommandAndWaitOK("AT\%CMNG=3,2222,1\r\n", MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setCertificate error");
        // return false;
    }

    char cmd[] = "AT\%CMNG=0,2222,1,\"";
    writeCommand(cmd);
    _pWrite((uint8_t *)client_ca, strlen(client_ca));
    _pWrite((uint8_t *)"\"\r\n", 3);

    if (!readResponseUntilOk(MODEM_SHORT_TIMEOUT))
        return false;

    _SecureMode = true;
    return true;
}

bool nRF9160Modem::setPrivateKey(const char *private_key)
{
    // delete the existing private key
    if (!writeCommandAndWaitOK("AT\%CMNG=3,2222,2\r\n", MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setPrivateKey error");
        // return false;
    }

    char cmd[] = "AT\%CMNG=0,2222,2,\"";
    writeCommand(cmd);
    _pWrite((uint8_t *)private_key, strlen(private_key));
    _pWrite((uint8_t *)"\"\r\n", 3);

    if (!readResponseUntilOk(MODEM_SHORT_TIMEOUT))
        return false;

    _SecureMode = true;
    return true;
}

bool nRF9160Modem::getVersion(std::string *response)
{
    if (response == nullptr)
        return false;

    std::string res;
    if (!writeCommandAndReadResponse("AT#XSLMVER\r\n", "#XSLMVER:", MODEM_LONG_TIMEOUT, &res))
    {
        // command error
        return false;
    }
    // #XSLMVER: "2.4.0","2.4.0-lte-788c5f11c0d6"

    *response = res.substr(10, res.length() - 10);
    return true;
}

bool nRF9160Modem::getlocaltime(std::tm *response, bool uselocal)
{
    if (response == nullptr || _activated == false)
        return false;

    std::string gmttime;
    if (!writeCommandAndReadResponse("AT+CCLK?\r\n", "+CCLK:", MODEM_SHORT_TIMEOUT, &gmttime))
        return false;

    // localtime=> +CCLK: "21/06/04,09:40:02+36" Year/Month/Day,Hour:Min:Sec+GMT(expressed in quarters of an hour)
    // resize=> 21/06/04,09:40:02
    std::string str = gmttime.substr(8, 17);

    std::tm tm = {};
    std::istringstream ss(str);
    ss >> std::get_time(&tm, "%y/%m/%d,%H:%M:%S");
    auto tp = std::chrono::system_clock::from_time_t(std::mktime(&tm));
    if (uselocal)
    {
        tp += std::chrono::hours(9); // to JST
    }
    auto t = std::chrono::system_clock::to_time_t(tp);
    localtime_r(&t, response);
    response->tm_year += 2000;

    return true;
}

bool nRF9160Modem::getIPaddress(std::string *response)
{
    if (response == nullptr || _activated == false)
        return false;

    std::string res;
    if (!writeCommandAndReadResponse("AT+CGPADDR=0\r\n", "CGPADDR:", MODEM_SHORT_TIMEOUT, &res))
    {
        // command error
        return false;
    }
    // ex. +CGPADDR: 1,"10.0.0.130","1050:0000:0000:0000:0005:0600:300c:326b"

    // return IPV4
    vector<string> vResp = split(res, ',');

    *response = vResp[1];
    return true;
}

bool nRF9160Modem::getUUID(std::string *response)
{
    if (response == nullptr)
        return false;

    std::string res;
    if (!writeCommandAndReadResponse("AT#XUUID\r\n", "#XUUID:", MODEM_SHORT_TIMEOUT, &res))
    {
        // command error
        return false;
    }
    // ex. #XUUID: 50503041-3633-4261-803d-1e2b8f70111a

    *response = res.substr(8, res.length() - 8);
    return true;
}

bool nRF9160Modem::getBand(uint8_t *band)
{
    if (band == nullptr || _activated == false)
        return false;

    std::string res;
    if (!writeCommandAndReadResponse("AT%XCBAND\r\n", "%XCBAND:", MODEM_SHORT_TIMEOUT, &res))
    {
        // command error
        return false;
    }
    // ex. %XCBAND: 13

    vector<string> vResp = split(res, ' ');
    *band = (uint8_t)atoi(vResp[1].c_str());

    return true;
}

bool nRF9160Modem::getCellRssi(int16_t *rssi)
{
    if (rssi == nullptr || _activated == false)
        return false;

    std::string res;
    if (!writeCommandAndReadResponse("AT%NBRGRSRP\r\n", "%NBRGRSRP:", MODEM_SHORT_TIMEOUT, &res))
    {
        // command error. at begging, it need take some time.
        *rssi = 0;
        return false;
    }
    // ex. %NBRGRSRP: 162,5900,51  51-140 is rssi

    if (res.length() > 0)
    {
        vector<string> vResp = split(res, ',');
        *rssi = (int16_t)atoi(vResp[2].c_str()) - 140;

        return true;
    }

    *rssi = 0;
    return false;
}

bool nRF9160Modem::getPhoneNumber(std::string *response)
{
    if (response == nullptr || _activated == false)
        return false;

    std::string res;
    if (!writeCommandAndReadResponse("AT+CNUM\r\n", "+CNUM:", MODEM_SHORT_TIMEOUT, &res))
    {
        // command error
        return false;
    }
    // ex. +CNUM: ,"xxxxxxxxx",129

    vector<string> vResp = split(res, ',');
    *response = vResp[1];

    return true;
}

bool nRF9160Modem::getIMSI(std::string *response)
{
    if (response == nullptr || _activated == false)
        return false;

    writeCommand("AT+CIMI\r\n");
    // ex. 123456789012345
    if (readLine(MODEM_SHORT_TIMEOUT, response))
    {
        return readResponseUntilOk(MODEM_SHORT_TIMEOUT);
    }

    return false;
}

bool nRF9160Modem::mqttOpen(const char *host, int port, const char *client_id, const char *username, const char *password)
{
    std::string response;

    if (_SecureMode)
    {
        snprintf(_cmd, CMD_BUFF_SIZE, "AT#XMQTTCON=1,\"%s\",\"%s\",\"%s\",\"%s\",%d,2222\r\n", client_id, username, password, host, port);
    }
    else
    {
        snprintf(_cmd, CMD_BUFF_SIZE, "AT#XMQTTCON=1,\"%s\",\"%s\",\"%s\",\"%s\",%d\r\n", client_id, username, password, host, port);
    }
    bool ret = writeCommand(_cmd);
    if (!ret)
        return false;
    ret = readResponse("XMQTTEVT", MODEM_ACTIVATE_TIMEOUT, &response, false);

    if (ret == TRUE)
    {
        vector<string> vResp = split(response, ',');

        int ret = atoi(vResp[1].c_str());
        if (ret == 0)
            return true;
        else
            return true;
    }
    else
    {
        return false;
    }
}

bool nRF9160Modem::mqttClose()
{
    std::string response;

    bool ret = writeCommand("AT#XMQTTCON=0");
    if (!ret)
        return false;
    ret = readResponse("XMQTTEVT", MODEM_LONG_TIMEOUT, &response, false);

    if (ret)
    {
        vector<string> vResp = split(response, ',');

        int result = atoi(vResp[1].c_str());
        if (result == 0)
            return true;
        else
            return false;
    }
    else
    {
        return false;
    }
}

bool nRF9160Modem::mqttSubscribe(const char *topic, int qos)
{
    snprintf(_cmd, CMD_BUFF_SIZE, "AT#XMQTTSUB=\"%s\",%d\r\n", topic, qos);

    std::string response;
    bool ret = writeCommand(_cmd);
    if (!ret)
        return false;
    ret = readResponse("XMQTTEVT: 7", MODEM_SHORT_TIMEOUT, &response, false);

    if (ret)
    {
        // response #XMQTTEVT: 7,0 or receive Message
        vector<string> vResp = split(response, ',');

        int result = atoi(vResp[1].c_str());
        if (result == 0)
            return true;
        else
            return false;
    }
    else
    {
        return false;
    }
}

bool nRF9160Modem::mqttUnSubscribe(const char *topic)
{
    snprintf(_cmd, CMD_BUFF_SIZE, "AT#XMQTTUNSUB=\"%s\"\r\n", topic);

    std::string response;
    bool ret = writeCommand(_cmd);
    if (!ret)
        return false;
    ret = readResponse("XMQTTEVT", MODEM_SHORT_TIMEOUT, &response, false);

    if (ret)
    {
        // response #XMQTTEVT: 8,0
        vector<string> vResp = split(response, ',');

        int result = atoi(vResp[1].c_str());
        if (result == 0)
            return true;
        else
            return false;
    }
    else
    {
        return false;
    }
}

bool nRF9160Modem::mqttPublish(const char *topic, const char *msg, int qos, int retain)
{
    snprintf(_cmd, CMD_BUFF_SIZE, "AT#XMQTTPUB=\"%s\",\"\",%d,%d\r\n", topic, qos, retain);

    if (!writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("mqtt pub error");
        return false;
    }

    // send data
    _pWrite((uint8_t *)msg, strlen(msg));

    // exit data mode
    writeCommand("+++");

    std::string response;
    if (!readResponse("#XDATAMODE", MODEM_SHORT_TIMEOUT, &response, false))
    {
        ERR_DEBUG_PRINTLN("socketSend error"); // No socket
        return false;                          // command timeout
    }

    return true;
}

int nRF9160Modem::mqttLoop()
{
    std::string response;
    bool loopFlag = false;
    int state = 0;
    int topic_len, msg_len, result, len;
    uint8_t rddata;
    uint8_t tmpBuf[64], tmpBuf_positon;

    Stopwatch sw;
    sw.Restart();

    do
    {
        switch (state)
        {
        case 0:
            len = _pRead(&rddata);
            if (len > 0)
            {
                // SerialDebug.write(rddata);

                loopFlag = true;
                response.push_back((char)rddata);

                if (rddata == '\n')
                {
                    if (response.length() > 2)
                    {
                        state = 1;
                    }
                    else
                    {
                        // SerialDebug.println("-CRLF-");
                        response = "";
                    }
                }
            }
            break;
        case 1:
            if (response.find("XMQTTMSG") != std::string::npos)
            {
                // #XMQTTMSG: 21,23
                vector<string> vResp = split(response, ' ');
                vector<string> vResp2 = split(vResp[1], ',');
                topic_len = atoi(vResp2[0].c_str());
                msg_len = atoi(vResp2[1].c_str());
                // SerialDebug.println("T:"+String(topic_len)+" M:"+ String(msg_len));
                state = 5;
                tmpBuf_positon = 0;
            }
            else if (response.find("XMQTTEVT") != std::string::npos)
            {
                // #XMQTTEVT: 2,0
                vector<string> vResp = split(response, ' ');
                vector<string> vResp2 = split(vResp[1], ',');
                // event = atoi(vResp2[0].c_str());
                result = atoi(vResp2[1].c_str());

                if (result < 0)
                {
                    return -1;
                }
                else
                {
                    loopFlag = false;
                }
            }
            else
            {
                return 0;
            }
            break;
        case 2:
            // read topic
            len = _pRead(&rddata);
            if (len > 0)
            {
                // SerialDebug.write(rddata);
                tmpBuf[tmpBuf_positon++] = rddata;
                if (tmpBuf_positon == topic_len)
                {
                    tmpBuf[topic_len] = 0;
                    tmpBuf_positon = 0;
                    mqttTopic = (char *)tmpBuf;
                    // SerialDebug.println("\r\nTop:"+mqttTopic);
                    state = 3;
                }
            }
            break;
        case 3:
            // SKIP CRLF
            len = _pRead(&rddata);
            if (len > 0)
            {
                // SerialDebug.write(rddata);
                tmpBuf_positon++;
                if (tmpBuf_positon == 2)
                {
                    tmpBuf_positon = 0;
                    state = 4;
                }
            }
            break;
        case 4:
            // read message
            len = _pRead(&rddata);
            if (len > 0)
            {
                // SerialDebug.write(rddata);
                tmpBuf[tmpBuf_positon++] = rddata;
                if (tmpBuf_positon == msg_len)
                {
                    tmpBuf[msg_len] = 0;
                    mqttMessage = (char *)tmpBuf;
                    // SerialDebug.println("\r\nmsg:"+mqttMessage);
                    return 1;
                }
            }
            break;
        case 5:
            len = _pRead(&rddata);
            if (len > 0)
            {
                // SerialDebug.write(rddata);
                if (rddata == '#')
                {
                    // #XMQTTMSG: again!?
                    state = 0;
                }
                else
                {
                    tmpBuf[tmpBuf_positon++] = rddata;
                    state = 2;
                }
            }
            break;
        }

        if (sw.ElapsedMilliseconds() >= MODEM_SHORT_TIMEOUT)
        {
            return -2;
        }
    } while (loopFlag);

    return 0;
}

bool nRF9160Modem::netServer(int port, Mode mode)
{
    _mode = mode;
    if (_mode == Mode::USE_TCP)
    {
        if (_SecureMode)
        {
            snprintf(_cmd, CMD_BUFF_SIZE, "AT#XTCPSVR=1,%d,2222\r\n", port);
        }
        else
        {
            snprintf(_cmd, CMD_BUFF_SIZE, "AT#XTCPSVR=1,%d\r\n", port);
        }
    }
    else
    {
        snprintf(_cmd, CMD_BUFF_SIZE, "AT#XUDPSVR=1,%d\r\n", port);
    }

    bool ret = writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT);

    if (ret)
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool nRF9160Modem::netServerClose()
{
    bool ret = false;
    if (_mode == Mode::USE_TCP)
    {
        ret = writeCommandAndWaitOK("AT#XTCPSVR=0\r\n", MODEM_SHORT_TIMEOUT);
    }
    else
    {
        ret = writeCommandAndWaitOK("AT#XUDPSVR=0\r\n", MODEM_SHORT_TIMEOUT);
    }

    if (ret)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool nRF9160Modem::netClientConnect(const char *host, int port, Mode mode)
{
    _mode = mode;

    if (_mode == Mode::USE_TCP)
    {
        snprintf(_cmd, CMD_BUFF_SIZE, "AT#XTCPCLI=1,\"%s\",%d\r\n", host, port);
    }
    else
    {
        snprintf(_cmd, CMD_BUFF_SIZE, "AT#XUDPCLI=1,\"%s\",%d\r\n", host, port);
    }

    bool ret = writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT);

    if (ret)
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool nRF9160Modem::netClientClose()
{
    bool ret = false;
    if (_mode == Mode::USE_TCP)
    {
        ret = writeCommandAndWaitOK("AT#XTCPCLI=0\r\n", MODEM_SHORT_TIMEOUT);
    }
    else
    {
        ret = writeCommandAndWaitOK("AT#XUDPCLI=0\r\n", MODEM_SHORT_TIMEOUT);
    }

    if (ret)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool nRF9160Modem::netSend(String msg)
{
    // SerialDebug.print(msg);
    return netSend((const uint8_t *)msg.c_str(), msg.length());
}

bool nRF9160Modem::netSend(const uint8_t *data, int len)
{
    // enter datamode
    if (_mode == Mode::USE_TCP)
    {
        if (!writeCommandAndWaitOK("AT#XTCPSEND\r\n", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("XTCPSEND error");
            return false;
        }
    }
    else
    {
        if (!writeCommandAndWaitOK("AT#XUDPSEND\r\n", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("XUDPSEND error");
            return false;
        }
    }

    // send data
    _pWrite((uint8_t *)data, len);

    // exit data mode
    writeCommand("+++");

    std::string response;
    if (!readResponse("#XDATAMODE", MODEM_SHORT_TIMEOUT, &response, false))
    {
        ERR_DEBUG_PRINTLN("netSend error"); // No socket
        return false;                       // command timeout
    }

    return true;
}

nRF9160Modem::State nRF9160Modem::netLoop(uint8_t *_data, int *max_len)
{
    std::string response;
    bool loopFlag = false;
    int state = 0;
    int msg_len, len;
    uint8_t rddata;
    int tmpBuf_positon;

    Stopwatch sw;
    sw.Restart();

    do
    {
        switch (state)
        {
        case 0:
            len = _pRead(&rddata);

            if (len > 0)
            {
                loopFlag = true;
                response.push_back((char)rddata);

                if (rddata == '\n')
                {
                    if (response.length() > 2)
                    {
                        state = 1;
                    }
                    else
                    {
                        response = "";
                        return NONE;
                    }
                }
            }
            break;
        case 1:
            // SerialDebug.println(response.c_str());
            if ((_mode == Mode::USE_TCP && response.find("XTCPDATA") != std::string::npos) || (_mode == Mode::USE_UDP && response.find("XUDPDATA") != std::string::npos))
            {

                vector<string> vResp = split(response, ' '); // ex. #XUDPDATA: 20 , #XTCPDATA: 445
                msg_len = atoi(vResp[1].c_str());
                state = 2;
                tmpBuf_positon = 0;
            }
            else if (response.find("ERROR") != std::string::npos)
            {
                return ERROR;
            }
            else if (response.find("disconnected") != std::string::npos)
            {
                // include client close=> disconnect
                return DISCONNECTED;
            }
            else if (response.find("connected") != std::string::npos)
            {
                // include client close=> disconnect
                vector<string> vResp = split(response, ' '); // ex. #XTCPSVR: "184.105.139.70","connected"
                vector<string> vResp2 = split(vResp[1], ',');

                *max_len = vResp2[0].length() - 2;
                memcpy(_data, &vResp2[0][1], *max_len); // copy ipaddress

                return CONNECTED;
            }

        case 2:
            // read data
            len = _pRead(&rddata);
            if (len > 0)
            {
                _data[tmpBuf_positon++] = rddata;
                if (tmpBuf_positon >= *max_len)
                {
                    // clear buffer
                    int tmpCount = msg_len - *max_len;
                    while (tmpCount)
                    {
                        len = _pRead(&rddata);
                        if (len > 0)
                        {
                            tmpCount--;
                        }
                        else
                        {
                            delay(1);
                        }
                    }
                    return BUFFOVERFLOW;
                }
                if (tmpBuf_positon == msg_len)
                {
                    *max_len = tmpBuf_positon;
                    return RECEIVED;
                }
            }
            break;
        }

        if (sw.ElapsedMilliseconds() >= MODEM_SHORT_TIMEOUT)
        {
            return ERROR;
        }
    } while (loopFlag);

    return NONE;
}